<?php if (!defined('TL_ROOT')) die('You can not access this file directly!');



$GLOBALS['TL_DCA']['tl_content']['palettes']['CPE-ImmobilienProKategorie'] = '{title_legend},type;{Feldzuordnungen},cpe_kategorie;';
$GLOBALS['TL_DCA']['tl_content']['fields']['cpe_kategorie'] = array(
	
	'label'                    => array("Kategorie","Wählen Sie die entsprechende Kategorie aus"),
	'inputType'                => 'select',
        'foreignKey'               => "tl_cpe_kategorien.name_de",
        
	
);



?>