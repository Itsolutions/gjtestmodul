<?php


$GLOBALS['TL_DCA']['tl_test_kategorien'] = array
(

	// Config
	'config' => array
	(
			'dataContainer'               => 'Table',
			'switchToEdit'                => true,
                     /*   'onload_callback' => array
                            (
                                    array('ContaoPortalEngineDCAHelper', 'loadLanguages')
                            ) */
			  'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary'
        
            )
        )


	),


	// Alle Einstellungen, die f�r die Auflistung bzw.
	// Manipulation von Datens�tzen ben�tigt werden.
	// Wir k�nnen hier die Sortierung, das Layout
	// (z. B. Filter, Suche, Einschr�nkungen)
	// Beschriftungen und Operationen bestimmen.

	'list' => array
	(
		// Sortierung
		'sorting' => array
		(
			'mode'                    => 1,
			// Sortierung nach name
			'fields'                  => array('name_de'),
			// Kategorisierung
			'flag'                    => 1,
			// Layout
			'panelLayout'             => 'sort,search,limit.'
		),

		// Beschriftung
		'label' => array
		(
 			'fields'                  => array('name_de'),
			'format'                  => '%s',

		),
	
	 	'global_operations' => array
		(
		
	 		'all' => array
			(
				 'label'               => &$GLOBALS['TL_LANG']['MSC']['all'],
				 'href'                => 'act=select',
	 			 'class'               => 'header_edit_all',
	 			'attributes'          => 'onclick="Backend.getScrollOffset();"'
			)

		),

	 	'operations' => array
		(
		
		
		
	 		'edit' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_kategorien']['edit'],
	 			'href'                => 'act=edit',
	 			'icon'                => 'edit.gif',
			),
			'copy' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_kategorien']['copy'],
	 			'href'                => 'act=copy',
	 			'icon'                => 'copy.gif',
			),
	 		'delete' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_kategorien']['delete'],
	 			'href'                => 'act=delete',
	 			'icon'                => 'delete.gif',
	 			'attributes'          => 'onclick="if (!confirm(\'' .
					$GLOBALS['TL_LANG']['MSC']['deleteConfirm'] . '\'))
	   			 return false; Backend.getScrollOffset();"',
			),
	 		'show' => array
			(
				'label'               => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_kategorien']['show'],
	 			'href'                => 'act=show',
	 			'icon'                => 'show.gif'
			)
		),
	), // list end

	
 	'palettes' => array
	(
		'default'                     => '{Allgemein},name_de,name_en,name_cn;'
										 
	),

 	'fields' => array
	(

        'id' => array(
            'sql'   =>   "int(10) unsigned NOT NULL auto_increment"
        ),

        'tstamp' => array
		(
			'sql'   => "int(10) unsigned NOT NULL default '0'"
		),




		// Felder, die im Backend angezeigt werden sollen.
		'name_de' => array
		(
			 'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_kategorien']['name_de'],
			 'inputType'               => 'text',
			 'search'                  => true,
			 'eval'                    => array('mandatory'=>true, 'multilang'=> 'true'),
			 'sql'                     => "varchar(255) NOT NULL default ''"
		),
		// Felder, die im Backend angezeigt werden sollen.
		'name_en' => array
		(
			 'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_kategorien']['name_en'],
			 'inputType'               => 'text',
			 'search'                  => true,
			 'eval'                    => array('mandatory'=>false, 'multilang'=> 'true'),
			 'sql'                     => "varchar(255) NOT NULL default ''"
		),
            
         // Felder, die im Backend angezeigt werden sollen.
		'name_es' => array
		(
			 'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_kategorien']['name_es'],
			 'inputType'               => 'text',
			 'search'                  => true,
			 'eval'                    => array('mandatory'=>false, 'multilang'=> 'true'),
			 'sql'                     => "varchar(255) NOT NULL default ''"
		),


		 // Felder, die im Backend angezeigt werden sollen.
		'name_fr' => array
		(
			 'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_kategorien']['name_fr'],
			 'inputType'               => 'text',
			 'search'                  => true,
			 'eval'                    => array('mandatory'=>false, 'multilang'=> 'true'),
			 'sql'                     => "varchar(255) NOT NULL default ''"
		),


		 // Felder, die im Backend angezeigt werden sollen.
		'name_ru' => array
		(
			 'label'                   => &$GLOBALS['TL_LANG']['MOD']['tl_cpe_kategorien']['name_ru'],
			 'inputType'               => 'text',
			 'search'                  => true,
			 'eval'                    => array('mandatory'=>false, 'multilang'=> 'true'),
			 'sql'                     => "varchar(255) NOT NULL default ''"
		),

		

	)
);






